#include "maximilian.h"
#include "queue"
maxiSample sample1; //samples
maxiSample sample2; //samples
maxiSample sample3; //samples
maxiSample sample4; //samples
maxiOsc LFO;//mod i fire
maxiOsc LFO2;//mod i fire
maxiOsc LFO3;//mod i fire
maxiFilter filter;//some filters low pass resonant filter
maxiOsc timer; //counting notes
maxiOsc filterOSC; //filter data
maxiEnv ADSR;
double mix = 0; //final sygnal
double sampleout1; //output sample
double sampleout2;
double sampleout3;
double sampleout4;
double LFOout; 
double LFOout2; 
double LFOout3; 
double ADSRout;
double filterout;
int currentCount;
//comb filter init
double numSamplesToDelay = 100; //try changing this; also called "L" in equation above
double poleRadius = 0.99; //try .99, .999, .9, etc. BUT NEVER EQUAL TO OR MORE THAN 1!!!!! Also called "R" in equation above
double rl = pow(poleRadius, numSamplesToDelay); // R ^ L
double nextInput = 0.0;
double nextOutput = 0.0;
std::queue<double> buff; //Hold the last L samples in a first-in-first-out queue

void setup() {
	ADSR.setAttack(10000); //ADSR
	ADSR.setDecay(1000);
	ADSR.setSustain(0.9);
	ADSR.setRelease(5000);
	//path to the file
	sample1.load("e:/year3/AdvancedAudioVisualProcessing/WEEK4/data/blip.wav"); 
	sample2.load("e:/year3/AdvancedAudioVisualProcessing/WEEK4/data/Cowbell.wav");
	sample3.load("e:/year3/AdvancedAudioVisualProcessing/WEEK4/data/Flick.wav");
	sample4.load("e:/year3/AdvancedAudioVisualProcessing/WEEK4/data/snare.wav");
	//checking if file is loaded
	printf("Check Sum :\n %s", sample1.getSummary());
	printf("Check Sum :\n %s", sample2.getSummary());
	printf("Check Sum :\n %s", sample3.getSummary());
	printf("Check Sum :\n %s", sample4.getSummary());
	//push zeros for filter array
	for (int i = 0; i < numSamplesToDelay; i++) {
		buff.push(0); //pre-populate queue with 0s
	}
}
void play(double *output) {
	currentCount = timer.phasor(0.3, 1, 21); //metronom
	if (currentCount == 1)
	{
		ADSR.trigger = 1;
	}
	else
	{
		ADSR.trigger = 0;
	}
	if(currentCount < 5) //magic down below
	{	
		LFOout = LFO.phasor(0.1); //modulate freq
		LFOout2 = LFO2.noise()*0.1;//add some noise
		sampleout4 = sample4.play((LFOout*0.7+LFOout2), 0, sample4.length/3)*ADSRout;//play shorter sample / 3 and add envelope
		sampleout2 = sample2.play(1, 0, sample2.length); //just play sample for 4 ticks
	}
	else if (currentCount >= 5 && currentCount <= 10)
	{	
		sampleout3 = sample3.play(5, 0, sample3.length)*ADSRout;
		sampleout1 = sample1.play(5.7, 0, sample1.length)*ADSRout;
		sampleout2 = sample2.play(3.8, 0, sample2.length)*ADSRout;
	}
	else if (currentCount >= 10 && currentCount <= 15)
	{
		LFOout3 = LFO3.sawn(0.5);
		sampleout2 = sample2.play(1.8, 0, sample2.length);
		sampleout3 = sample3.play(-15+ LFOout3*9, 0, sample3.length*LFOout3)*ADSRout;//play reverse and modulate freq and amp, adjust lenght and play envelope
		sampleout1 = sample1.play(1.7, 0, sample1.length)*ADSRout;
	}
	else if (currentCount >= 15)
	{
		sampleout1 = sample1.play(-4.7, 0, sample1.length);
		sampleout2 = (sample2.play(12.8+ LFOout3*5, 0, sample2.length*LFOout3)*ADSRout);
		sampleout3 = sample3.play(-6.5, 0, sample3.length)*ADSRout;
		sampleout4 = sample4.play(LFOout3, 0, sample4.length*ADSRout);
	}
	ADSRout = ADSR.adsr(1., ADSR.trigger);
	mix = (sampleout1 + sampleout2 + sampleout4 + sampleout3)*0.25;
	filterout = filter.lores((mix)*(filterOSC.saw(LFOout) + filterOSC.pulse(1, LFO.phasor(LFOout))), 9000, 2);//resonant low pass filter at 9000 hz
	nextInput = filterout;
	nextOutput = nextInput + rl * buff.front(); //Implements y[n] = x[n] + rl * y[n-numSamplesToDelay]
	output[0] = (nextOutput) * 0.5; //left mono
	output[1] = (nextOutput) * 0.5; //right mono
	buff.push(nextOutput); //Put y[n] onto queue (will become y[n-1] next time)
	buff.pop(); //Remove y[n-L] from the queue
	ADSR.trigger = 0;
}